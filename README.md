# Paquete de utilidades de bpanel4

---
Paquete de utilidades varias para gestionar componentes que se usan a lo largo de los distintos paquetes de bpanel4.

## Datatable Actions Buttons

En el siguiente ejemplo se muestra la tabla de language (idiomas):

```php
->addColumn('action', function (LanguageModel $language) {
                return view('utils::partials.datatable-button')->with([
                    'buttons' => [
                        'show', 'edit', 'delete'
                    ],
                    'model' => $language,
                    'scope' => 'language'
                ]);
            });
```

### Parámetros para las rutas de datatable action buttons

En algunos casos, puede ser necesario pasar parámetros para construir las rutas
que necesitan los botones de editar y borrar de los datatables. Para pasar estos
parámetros, se usa el índice `routeParams`, en el que podremos definir estos
parámetros para cada ruta, como se ve en el siguiente ejemplo (pasamos a la ruta
`edit` los parámetros `moduleName` y `tagType`:

```php 
@livewire('utils::datatable-action-buttons', [
    'actions' => ["edit", "delete"],
    'scope' => 'related-tags',
    'model' => $row,
    'permission' => ['edit', 'delete'],
    'id' => $row->id,
    'message' => 'la etiqueta?',
    'routeParams' => [
        'edit' => [
            'moduleName' => $moduleName,
            'tagType' => $tagType,
        ]
    ]
], key('tag-buttons-'.$row->id))
```

En config/utils.php está la configuración de cada botón. Se pueden añadir más botones...

```php
'datatable-buttons-themes' => [
        'show' => [
            'color' => 'blue',
            'icon' => 'fa fa-search',
            'title' => 'Ver',
            'margin' => 'mx-1'
        ],
        'edit' => [
            'color' => 'green',
            'icon' => 'fa fa-pencil',
            'title' => 'Editar',
            'margin' => 'mx-1'
        ],
        'delete' => [
            'color' => 'danger',
            'icon' => 'fa fa-trash',
            'title' => 'Borrar',
            'margin' => 'mx-1',
            'deleteForm' => true,
        ]
    ]
```

Botón de activado / desactivado
```php
->addColumn('active', function(LanguageModel $language){
                return view ('utils::partials.datatable-checkbox')->with([
                    'model' => $language,
                    'fieldName' => 'active',
                ]);
            })
```

NOTA: Para que funcione los botones por livewire es obligatorio añadir esta directiva en la función html():
```php
$this->builder()->drawCallbackWithLivewire();
```


## Credits

- [javimg](https://github.com/bittacora)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
