<?php

return [
    'datatable-buttons-themes' => [
        'show' => [
            'color' => 'blue',
            'icon' => 'fa fa-search',
            'title' => 'Ver',
            'margin' => '',
            'size' => 'xxs',
        ],
        'edit' => [
            'color' => 'green',
            'icon' => 'fa fa-pencil',
            'title' => 'Editar',
            'margin' => '',
            'size' => 'xxs',
        ],
        'destroy' => [
            'color' => 'danger',
            'icon' => 'fa fa-trash',
            'title' => 'Borrar',
            'margin' => '',
            'deleteForm' => true,
            'size' => 'xxs',
        ],
        'pdf' => [
            'color' => 'danger',
            'icon' => 'fa fa-pdf',
            'title' => 'pdf',
            'margin' => '',
            'size' => 'xxs',

        ]
    ]
];
