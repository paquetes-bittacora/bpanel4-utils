<?php

return [
    'configuration' => 'Configuración SEO',
    'slug' => 'Url amigable',
    'meta_title' => 'Meta título',
    'meta_description' => 'Meta descripción',
    'meta_keywords' => 'Palabras clave'
];
