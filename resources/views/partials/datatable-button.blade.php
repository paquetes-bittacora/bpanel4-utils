<div class="action-buttons">
    @foreach ($buttons as $key => $button)
        @can($scope.".".$button)
        @livewire('utils::datatable-button',[
            'model' => $model,
            'scope' => $scope,
            'button' => $button
        ])
        @endcan
    @endforeach
</div>
