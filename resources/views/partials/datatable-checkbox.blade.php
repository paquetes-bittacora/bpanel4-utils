<div class="text-center">
    @livewire('utils::datatable-checkbox',[
    'model' => $model,
    'fieldName' => $fieldName,
    'permission' => $permission
    ])
</div>
