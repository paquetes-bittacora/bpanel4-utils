<div class="text-center">
    @livewire('utils::datatable-default',[
    'model' => $model,
    'fieldName' => $fieldName,
    'permission' => $permission,
    'reload' => $reload,
    'size' => $size,
 ])
</div>
