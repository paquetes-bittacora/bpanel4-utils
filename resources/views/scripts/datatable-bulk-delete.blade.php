<script>
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
        }
    });

    $(document).ready(function(){
        $("body").on("change", ".checkbox-bulk-delete", function(){
            if($(this).is(":checked")){
                $(".delete-checkbox").prop("checked", true);
            }else{
                $(".delete-checkbox").prop("checked", false);
            }

            if($(".delete-checkbox:checked").length > 0){
                $(".bulk-delete-button").removeClass("d-none");
            }else{
                $(".bulk-delete-button").addClass("d-none");
            }
        });

        $("body").on("change", ".delete-checkbox", function(){
            if($(".delete-checkbox:checked").length > 0){
                $(".bulk-delete-button").removeClass("d-none");
            }else{
                $(".bulk-delete-button").addClass("d-none");
            }
        });

        $("body").on("click", ".bulk-delete-button", function(){
            if($(".delete-checkbox:checked").length > 0){
                Swal.fire({
                    title: '¡Atención!',
                    text: '¿Desea eliminar los elementos seleccionados?',
                    icon: 'warning',
                    type: "warning",
                    confirmButtonText: 'Eliminar',
                    confirmButtonColor: '#e3342f',
                    cancelButtonText: 'Cancelar',
                    showCancelButton: true,
                    showLoaderOnConfirm: true,
                    preConfirm: () => {
                        var idsToDelete = [];
                        $(".delete-checkbox:checked").each(function(key, value){
                            idsToDelete.push($(value).val());
                        });

                        $.ajax({
                            type: "post",
                            url: "{{$bulkDeleteRoute}}",
                            dataType: "json",
                            data: {ids : idsToDelete},
                            success: function (msg) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Confirmación',
                                    text: 'Los elementos seleccionados han sido eliminados correctamente.',
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            },
                            error: function(msg){
                                Swal.fire({
                                    title: '¡ERROR!',
                                    icon: 'error',
                                    text: 'Hubo un error al eliminar uno o más elementos.',
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            },
                        }).always(function(){
                            $(".dataTable").DataTable().ajax.reload();
                            $(".bulk-delete-button").addClass("d-none");
                            $(".checkbox-bulk-delete").prop("checked", false);
                        });
                    }
                });
            }else{
                Swal.fire({
                    title: '¡Error!',
                    text: 'Debe seleccionar, al menos, un elemento de la tabla.',
                    type: 'error',
                    icon: 'error',
                    confirmButtonText: '¡Entendido!',
                });
            }

        });
    });
</script>
