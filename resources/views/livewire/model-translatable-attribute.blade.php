<div>
@foreach(\Bittacora\Language\LanguageFacade::getActives() as $language)
    <div class="d-flex align-items-center mb-2">
        <img src="{{asset("assets_bpanel/flags/$language->locale.png")}}" class="mr-2">
        {{$model->getTranslation($attribute, $language->locale)}}
    </div>
@endforeach
</div>
