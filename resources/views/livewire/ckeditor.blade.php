<div>
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            {{ $labelText }}
        </div>
        <div class="col-sm-7">
        <textarea id="{{ $name }}" name="{{ $name }}" >
            {!! $value ?? '' !!}
        </textarea>
        </div>
    </div>

    <style>
        /* @todo-jose sacar esto a una hoja de estilos */
        .ck-editor__editable_inline {
            min-height: 20rem;
        }
    </style>

    @push('scripts')
        <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
        <script>
            ClassicEditor
                .create(document.querySelector('#{{ $name }}'))
                .then(editor => {
                    window.editor = editor;
                })
                .catch(error => {
                    console.error('Ocurrió un problema al cargar el editor.', error);
                });

        </script>
    @endpush
</div>
