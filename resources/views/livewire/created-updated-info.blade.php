@if(!is_null($createdBy) and !is_null($createdAt) and !is_null($updatedBy) and !is_null($updatedAt))
    <div>
        <div class="card-header bgc-primary-d1 text-white border-0 mb-0">
            <h4 class="text-120 mb-0">
                <span class="text-90">{{ __('utils::form.created_updated_info') }}</span>
            </h4>
        </div>
        <div class="card-body">
            @livewire('form::input-text', ['name' => 'created_at_created_by', 'value' => $createdAt . ' - ' . $createdBy, 'disabled' => true, 'labelText' => 'Fecha de creación'])
            @livewire('form::input-text', ['name' => 'updated_at_updated_by', 'value' => $updatedAt . ' - ' . $updatedBy, 'disabled' => true, 'labelText' => 'Última actualización'])
        </div>
    </div>
@endif
