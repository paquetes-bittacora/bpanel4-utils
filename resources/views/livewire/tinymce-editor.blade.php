<div>
    <div class="form-group form-row">
        <div class="col-sm-{{$labelWidth}} col-form-label text-sm-right">
            {{ $labelText }}
        </div>
        <div class="col-sm-{{$fieldWidth}}">
        <textarea id="{{ $name }}" name="{{ $name }}" class="tinymce-editor-bp4">
            {!! $value ?? '' !!}
        </textarea>
        @error($name)
            <div class="text-danger">{{ $message }}</div>
        @enderror
        </div>
    </div>
</div>
