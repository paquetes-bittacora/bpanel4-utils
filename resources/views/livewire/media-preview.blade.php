<div class="multimedia-preview text-center">
    @if(!is_null($media))
        <a href="{{$media->getUrl()}}" class="d-block text-center" data-fancybox="multimedia{{$media->id}}">
            <img src="{{$media->getUrl('thumb-preview')}}" alt="">
        </a>
    @endif
</div>
