@can($permission)
    @if (isset($params['deleteForm']))
        <form method="POST"
              class="d-inline {{$params['margin']}}"
              action="{{route($route, $model)}}">
            {!!method_field('delete')!!}
            @csrf
            <button
                type="submit"
                class="btn btn-{{$params['size']}} btn-outline-{{$params['color']}}"
                onclick="return confirm('¿Quieres borrar este contenido?')"
                @if(isset($params['disabled'])) disabled @endif>
                <i class="fa fa-trash"></i>
            </button>
        </form>
    @else
        <a class="btn btn-{{$params['size']}} btn-outline-{{$params['color']}} {{$params['margin']}}" href="{{route($route,$model)}}" title="{{$params['title']}}">
            <i class="{{$params['icon']}}"></i>
        </a>
    @endif
@endcan
