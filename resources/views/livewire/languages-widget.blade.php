<div class="@if($layout == 'inline') d-inline-block @elseif($layout =='flex') d-flex @elseif($layout == 'block') d-block @endif">
    @if($format == 'flags')
        @foreach(\Bittacora\Language\LanguageFacade::getActives() as $language)
            <div class="flag">
                <img src="{{asset("assets_bpanel/flags/$language->locale.png")}}" alt="{{$language->name}}">
            </div>
        @endforeach
    @elseif($format == 'text')
            <div class="country">
                <span>{{strtoupper($language->locale)}}</span>
            </div>
    @endif
</div>
