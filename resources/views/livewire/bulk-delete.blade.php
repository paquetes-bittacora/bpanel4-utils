<div clasS="mt-4 mb-4 @if(!count($models)) d-none @endif">
    <button class="btn btn-danger" wire:click="bulkDeleteConfirmation">Eliminar seleccionados ({{count($models)}})</button>
</div>

@push('scripts')
    <script>
        window.addEventListener('updateDatatable', function(){
            $(".dataTable").DataTable().ajax.reload();
            Swal.fire({
                title: 'Los recursos han sido eliminados correctamente',
                icon: 'success',
                timer: 1500
            });
        });

        window.addEventListener('bulkDeleteConfirmation', function(){
            Swal.fire({
                title: '¿Estás seguro de querer eliminar los elementos seleccionados?',
                text: 'Esta acción es irreversible.',
                showCancelButton: true,
                confirmButtonText: 'Sí',
                cancelButtonText: 'Cancelar',
                customClass: {
                    confirmButton: 'bg-danger'
                }
            }).then(function(result){
                if (result.isConfirmed) {
                    Livewire.emit('bulkDelete');
                }
            });
        });
    </script>
@endpush
