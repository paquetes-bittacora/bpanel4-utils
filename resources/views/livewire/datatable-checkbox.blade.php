<div>
    <div class="form-check form-check-inline">
        <input class="form-check-input ace-switch input-sm"
               type="checkbox"
               wire:click="toggle"
               wire:model="value"
               @cannot($permission) disabled @endcannot>
    </div>
</div>
