<div class="action-buttons">
{{--    <a class="text-blue mx-2" href="{{route($scope.'.show',$model)}}">--}}
{{--        <i class="fa fa-search"></i>--}}
{{--    </a>--}}
    @if(in_array('edit', $actions))
    <a class="text-success mx-2" href="{{route($scope.'.edit', (isset($routeParams['edit']) ? $routeParams['edit'] + ['model' => $model] : $model ))}}">
        <i class="fa fa-pencil"></i>
    </a>
    @endif

    @if(in_array('delete', $actions))
    <form method="POST" class="d-inline mx-2"
          action="{{route($scope.'.destroy', (isset($routeParams['destroy']) ? $routeParams['destroy'] + ['model' => $model] : $model ))}}">
        {!!method_field('DELETE')!!}
        @csrf
        <button id="delete_button{{$model->id}}" type="submit" class="btn btn-primary-outline text-danger"
                @if($disabled) disabled @endif
                onclick="return confirm('¿Está seguro de querer borrar {{$message}}')">
            <i class="fa fa-trash"></i>
        </button>
    </form>
    @endif

    @if(in_array('invoice', $actions))
        <a class="text-info mx-2" target="_blank" href="{{route($scope.'.invoice', $model)}}">
            <i class="fa fa-file" aria-hidden="true"></i>
        </a>
    @endif
</div>
