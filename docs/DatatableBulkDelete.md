<h1>Funcionamiento del borrado por lotes</h1>
<h3 style="color:greenyellow; font-weight:400">(Datatable bulk delete)</h3>

<p>
    Para poder añadir el borrado por lotes a una 
    datatable debemos seguir los siguientes pasos:
</p>

<ol>
    <li style="margin-bottom:15px">
        <strong>Añadir una columna a la datatable: </strong> 
        tenemos que añadir una nueva columna donde colocaremos cada uno 
        de los checkbox cuyo valor será el id del modelo para cada elemento.
        Para ello, se ha creado un componente simple de livewire para generar checkboxes
        que recibe unos pocos parámetros. Os dejo el código de ejemplo usado en 
        el datatable de Multimedia.
        <code>return view('utils::livewire.datatable-checkbox-delete')->with(['value' => $multimedia->id, 'name' => 'ids[]', 'className' => 'delete-checkbox']);</code>
    </li>
    <li style="margin-bottom:15px">
        <strong>Añadir el checkbox para la cabecera de la columna:</strong>
        en la función <code>getColumns()</code> de la datatable agregaremos el siguiente código
        dentro del return <code>Column::make('bulkDelete')->title('<input type="checkbox" class="checkbox-bulk-delete">')
                ->width("50px")
                ->orderable(false)
                ->searchable(false)
                ->className('text-center')</code></li>

<li style="margin-bottom:15px">
    <strong>Añadir el botón: </strong>a continuación, deberemos añadir el botón
del borrado por lotes a nuestra datatable. Para incluirlo, añadiremos al return del builder en
la función <code>html()</code> de la datatable la función <code>parameters()</code> 
indicando el botón a incluir de la siguiente forma. <code>
$this->builder()->parameters(['buttons' => [
                    [
                        'text' =>'<i class="fa fa-trash"></i> ' . 'Eliminar seleccionados',
                        'className' => 'bulk-delete-button bg-danger border-none d-none'
                    ],
                ]
            ])</code>
</li>
<li style="margin-bottom:15px">
    <strong>Incluir el script: </strong> comprobando que el usuario tenga el permiso
para poder utilizar el borrado por lotes, incluiremos el BLADE que contiene todos los 
scripts que controlan este comportamiento, usaremos la directiva <code>@include</code>
de blade para añadir el script. 
<code>
    @can('multimedia.bulkdelete')
        @include('utils::scripts.datatable-bulk-delete')
    @endcan
</code>
</li>

<li style="margin-bottom: 15px">
    <strong>Ruta: </strong>tendremos que añadir la ruta correspondiente en nuestro
paquete. Dejo de ejemplo la que he utilizado en multimedia.
<code>Route::post('/multimedia/bulkdelete', [MultimediaController::class, 'bulkDelete'])->name('bulkdelete');</code>
</li>

<li style="margin-bottom:15px">
    <strong>Ruta por parámetro: </strong>a la hora de renderizar nuestra 
tabla desde el controlador, hay que pasarle la ruta como parámetro a la vista.
<code>return $multimediaDatatable->render('multimedia::index', ['bulkDeleteRoute' => route('multimedia.bulkdelete')]);</code>
</li>

<li style="margin-bottom:15px">
    <strong>Método "bulkDelete": </strong>ya solo faltaría indicar el comportamiento
de nuestro método <code>bulkDelete(Request $request)</code> en nuestro controlador.
</li>
</ol>

<pre>
    Es importante no olvidar crear el permiso correspondiente para la acción <code>bulkdelete</code>.
</pre>
