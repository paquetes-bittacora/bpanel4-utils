<?php

namespace Bittacora\Utils;

use Bittacora\Multimedia\DataTables\MultimediaDatatable;
use Bittacora\Utils\Http\Livewire\Ckeditor;
use Bittacora\Utils\Http\Livewire\CreatedUpdatedInfo;
use Bittacora\Utils\Http\Livewire\DatatableButton;
use Bittacora\Utils\Http\Livewire\DatatableCheckbox;
use Bittacora\Utils\Http\Livewire\DatatableDefault;
use Bittacora\Utils\Http\Livewire\DatatablePolyDefault;
use Bittacora\Utils\Http\Livewire\FormatIcon;
use Bittacora\Utils\Http\Livewire\LanguagesWidget;
use Bittacora\Utils\Http\Livewire\MediaLibraryImages;
use Bittacora\Utils\Http\Livewire\MediaPreview;
use Bittacora\Utils\Http\Livewire\ModelTranslatableAttribute;
use Bittacora\Utils\Http\Livewire\TinymceEditor;
use Livewire\Livewire;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;
use Bittacora\Utils\Http\Livewire\DatatableActionButtons;

class UtilsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('utils')
            ->hasConfigFile()
            ->hasViews();
    }

    public function register(){
        $this->app->bind('utils', function($app){
            return new Utils();
        });
    }

    public function boot(){
        $this->loadViewsFrom(__DIR__. '/../resources/views', 'utils');
        $this->loadTranslationsFrom(__DIR__ .'/../resources/lang', 'utils');

        Livewire::component('utils::datatable-checkbox', DatatableCheckbox::class);
        Livewire::component('utils::datatable-default', DatatableDefault::class);
        Livewire::component('utils::datatable-poly-default', DatatablePolyDefault::class);
        Livewire::component('utils::datatable-button', DatatableButton::class);
        Livewire::component('utils::media-preview', MediaPreview::class);
        Livewire::component('utils::format-icon', FormatIcon::class);
        Livewire::component('utils::ckeditor', Ckeditor::class);
        Livewire::component('utils::created-updated-info', CreatedUpdatedInfo::class);
        Livewire::component('utils::languages-widget', LanguagesWidget::class);
        Livewire::component('utils::model-translatable-attribute', ModelTranslatableAttribute::class);
        Livewire::component('utils::media-library-images', MediaLibraryImages::class);
        Livewire::component('utils::tinymce-editor', TinymceEditor::class);
        Livewire::component('utils::datatable-action-buttons', DatatableActionButtons::class);

        $this->publishes([
            __DIR__.'/../config/utils.php' => config_path('utils.php'),
        ], 'package-name');

        // Publico el JS de CKEditor
        $this->publishes([
            __DIR__ . '/../node_modules/@ckeditor/ckeditor5-build-classic/build/' => public_path('vendor/ckeditor'),
        ], 'public');
    }

}
