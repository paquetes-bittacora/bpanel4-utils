<?php

namespace Bittacora\Utils;

use Carbon\Traits\Timestamp;
use Illuminate\Support\Carbon;
use phpDocumentor\Reflection\Types\Collection;

class Utils
{
    public function getModulePermissions(string $modulePrefix)
    {
        return \Spatie\Permission\Models\Permission::all()->filter(function ($value, $key) use($modulePrefix) {
            return strpos($value->name,$modulePrefix) === 0;
        });
    }

    /**
     * Convierte un tamaño en bytes a una magnitud superior dependiendo del tamaño pasado por parámetro
     * @param mixed $size Tamaño en bytes del fichero
     * @return string
     */
    public function convertFromBytes($size){
        if($size > 1073741824){
            return str_replace(".", ",", number_format($size / 1073741824, 2)) . ' GB';
        }elseif($size > 1048576){
            return str_replace(".", ",", number_format($size / 1048576, 2)) . ' MB';
        }else{
            return str_replace(".", ",", number_format($size / 1024, 2)) . ' KB';
        }
    }

    /**
     * Extrae la extensión del archivo a través del filename del modelo Media
     *
     * @param string $filename
     * @return mixed|string
     */
    public function extractExtensionFromFilename(string $filename){
        $filenameArray = explode('.', $filename);

        return end($filenameArray);
    }

    /**
     * Transforma la fecha de formato de Timestamp a formato español
     */
    public function timestampToSpanishFormat(string $timestamp){
        return Carbon::parse($timestamp)->format('d/m/Y H:i:s');
    }
}
