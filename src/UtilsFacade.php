<?php

namespace Bittacora\Utils;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Bittacora\Language\Language
 */
class UtilsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'utils';
    }
}
