<?php


namespace Bittacora\Utils\DataTable;

trait Reorder
{
    /**
     * @param String $route
     * @return string
     */
    public function printJavascriptReorder(String $route) : string
    {
        $idTable = $this->html()->getTableAttributes()['id'];

        return '
        <script type="text/javascript">
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                "X-CSRF-TOKEN": $("meta[name=\"csrf-token\"]").attr("content")
                }
            });
            var table = window.LaravelDataTables["'.$idTable.'"];
            table.on( "row-reorder", function ( e, diff, edit ) {

                var myArray = [];
                for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                    var rowData = table.row( diff[i].node ).data();
                    myArray.push({
                        id: rowData.id,   // record id from datatable
                        position: diff[i].newPosition+1 // new position
                    });
                }
                var jsonString = JSON.stringify(myArray);
                $.ajax({
                    url     : "'.route($route).'",
                    type    : "POST",
                    data    : jsonString,
                    dataType: "json",
                });
            });

        });
    </script>';
    }
}
