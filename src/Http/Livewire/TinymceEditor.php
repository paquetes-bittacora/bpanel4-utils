<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;
use function view;

class TinymceEditor extends Component
{
    public $name;
    public $idField;
    public $labelWidth = 3;
    public $labelText;
    public $fieldWidth = 9;
    public $required = false;
    public $value = null;

    public function render()
    {
        return view('utils::livewire.tinymce-editor');
    }
}
