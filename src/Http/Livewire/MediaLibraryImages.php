<?php

namespace Bittacora\Utils\Http\Livewire;

use Bittacora\Language\Models\LanguageModel;
use Bittacora\Multimedia\Models\Multimedia;
use Bittacora\Utils\DataTable\Reorder;
use Livewire\Component;
use Livewire\WithPagination;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Options\Plugins\RowReorder;
use Yajra\DataTables\Services\DataTable;

class MediaLibraryImages extends Component
{
    use WithPagination;
    public $perPage = 5;

    public function render(){
        return view('utils::livewire.media-library', [
            'media' => Multimedia::paginate($this->perPage)
        ]);
    }
}
