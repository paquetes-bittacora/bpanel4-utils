<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class DatatablePolyDefault extends Component
{
    public $fieldName = 'activate';
    /**
     * @var string Nombre de la relación, es decir el prefijo de los campos XXXX_type y XXXX_id de la tabla donde se
     *             guarda la relación..
     */
    public $relationName;
    public $model;
    public $value;
    public $permission = null;

    /**
     * Nombre de la ruta a la que redirigir cuando se cambie el estado. Se puede usar, por ejemplo
     * Illuminate\Support\Facades\Route::currentRouteName() para usar la ruta en la que se está renderizando el
     * template que llama a este componente de Livewire.
     */
    public string $reloadRouteName = '';

    /**
     * Array de parámetros que se pasarán para formar la ruta a la que se redirigirá una vez se cambie el estado. Para
     * obtener las de la ruta en la que se está renderizando el template que llama a este componente, se puede usar
     * (Illuminate\Support\Facades\Route::getCurrentRoute())->originalParameters()
     * @var array<string, mixed>
     */
    public array $reloadRouteParams = [];
    public $size = 'xxs';

    public function mount(Model $model)
    {
        $this->model = $model;
        $this->value = $model->{$this->fieldName};
    }

    public function render()
    {
        return view('utils::livewire.datatable-poly-default')->with([
            'model' => $this->model,
            'value' => $this->value,
            'size' => $this->size
        ]);
    }

    public function toggle()
    {
        if (!Auth::user()->can($this->permission)) {
            $this->alert('alert', 'No tienes permiso');
            return false;
        }

        $this->setAllToFalse();
        $this->setCurrentItemToTrue();

        $this->value = $this->model->{$this->fieldName};
        if (!empty($this->reloadRouteName)) {
            return redirect()->to(route($this->reloadRouteName, $this->reloadRouteParams));
        }
    }

    private function setAllToFalse(): void
    {
        $parent = $this->model->{$this->relationName};

        $relatedModels = (get_class($this->model))::where($this->relationName . '_type', get_class($parent))
            ->where($this->relationName . '_id', $parent->id)->get();

        foreach ($relatedModels as &$item) {
                $item->{$this->fieldName} = false;
                $item->save();
            };
    }

    private function setCurrentItemToTrue(): void
    {
        $this->model->{$this->fieldName} = true;
        $this->model->save();
    }
}
