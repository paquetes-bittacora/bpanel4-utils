<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class BulkDeleteCheckbox extends Component
{
    public $value;
    public $modelName;

    protected $listeners = ['sendRecordToBulkDelete'];

    public function render()
    {
        return view('utils::livewire.bulk-delete-checkbox');
    }

    public function sendRecordToBulkDelete(){
        $this->emit('addBulkDeleteRecord', $this->value, $this->modelName);
    }
}
