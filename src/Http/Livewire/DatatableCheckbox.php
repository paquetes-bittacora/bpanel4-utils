<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class DatatableCheckbox extends Component
{
    public $fieldName;
    public $value;
    public $model;
    public $permission;

    public function mount(Model $model){
        $this->model = $model;
        $this->value = $model->{$this->fieldName};
    }

    public function render()
    {
        return view('utils::livewire.datatable-checkbox')->with([
            'fieldName' => $this->fieldName,
            'value' => $this->value,
        ]);
    }

    public function toggle(){
        if (!Auth::user()->can($this->permission)){
            $this->alert('alert', 'No tienes permiso');
            $this->value = !$this->value;
            return false;
        }
        $this->model->update([$this->fieldName => $this->value]);
        $this->value = $this->model->{$this->fieldName};
    }
}
