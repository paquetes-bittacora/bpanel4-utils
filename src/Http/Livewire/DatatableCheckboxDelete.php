<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class DatatableCheckboxDelete extends Component
{
    public string $value;
    public string $name;
    public ?string $className = null;

    public function render()
    {
        return view('utils::livewire.datatable-checkbox-delete')->with([
            'value' => $this->value,
            'name' => $this->name,
            'className' => $this->className
        ]);
    }
}
