<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class ModelTranslatableAttribute extends Component
{

    public bool $flags = true;
    public ?string $attribute = null;
    public Model $model;

    protected $listeners = ['jajaxd'];

    public function jajaxd(){
        $this->mount();
    }

    public function render()
    {
        return view('utils::livewire.model-translatable-attribute')->with([
            'flags' => $this->flags,
            'attribute' => $this->attribute,
            'model' => $this->model
        ]);
    }
}
