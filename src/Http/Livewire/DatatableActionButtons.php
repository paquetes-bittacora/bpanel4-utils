<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class DatatableActionButtons extends Component
{
    public $actions;
    public $model;
    public $scope;
    public $permission;
    public $route;
    public $disabled;
    public $message;
    public $routeParams;

    public function render()
    {
        return view('utils::livewire.datatable-action-buttons');
    }
}
