<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class MediaPreview extends Component
{
    public $media;

    public function render()
    {
        return view('utils::livewire.media-preview')->with([
            'media' => $this->media
        ]);
    }
}
