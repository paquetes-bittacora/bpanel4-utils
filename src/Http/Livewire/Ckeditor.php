<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class Ckeditor extends Component
{
    public $name;

    public $labelText;
    
    public $value;
    
    public function render()
    {
        return view('utils::livewire.ckeditor');
    }
}
