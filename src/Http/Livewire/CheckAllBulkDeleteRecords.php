<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class CheckAllBulkDeleteRecords extends Component
{
    public $selectAll = false;

    protected $listeners = ['check'];

    public function render()
    {
        $this->dispatchBrowserEvent('toggleCheck');
        return view('utils::livewire.check-all-bulk-delete-records');
    }

    public function check(){
        dd("hola");
    }
}
