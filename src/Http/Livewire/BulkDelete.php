<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class BulkDelete extends Component
{
    public $models = [];

    protected $listeners = ['addBulkDeleteRecord', 'bulkDelete'];

    public function render()
    {
        return view('utils::livewire.bulk-delete');
    }

    public function addBulkDeleteRecord($id, $modelName){
        if(array_key_exists($id, $this->models)){
            unset($this->models[$id]);
        }else{
            $this->models[$id] = $modelName;
        }
    }

    public function bulkDeleteConfirmation(){
        $this->dispatchBrowserEvent('bulkDeleteConfirmation');
    }

    public function bulkDelete(){
        foreach($this->models as $id => $modelName){
            $model = $modelName::findOrFail($id);

            if($model->delete()){
                unset($this->models[$id]);
            }

        }

        $this->dispatchBrowserEvent('updateDatatable');
    }
}
