<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class DatatableDefault extends Component
{
    public $fieldName='activate';
    public $model;
    public $value;
    public $permission = null;
    public $reload = null;
    public $size= 'xxs';

    public function mount(Model $model){
        $this->model = $model;
        $this->value = $model->{$this->fieldName};
    }

    public function render()
    {
        return view('utils::livewire.datatable-default')->with([
            'model' => $this->model,
            'value' => $this->value,
            'size' => $this->size
        ]);
    }

    public function toggle(){
        if (!Auth::user()->can($this->permission)){
            $this->alert('alert', 'No tienes permiso');
            $this->value = !$this->value;
            return false;
        }
        $this->model->{$this->fieldName} = !$this->model->{$this->fieldName};
        $this->model->save();
        $this->value = $this->model->{$this->fieldName};
        if (!empty($this->reload)){
            return redirect()->to(route($this->reload));
        }
    }
}
