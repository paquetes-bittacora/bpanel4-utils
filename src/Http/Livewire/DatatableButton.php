<?php

namespace Bittacora\Utils\Http\Livewire;

use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class DatatableButton extends Component
{
    public $params;
    public $model;
    public $scope;
    public $permission;
    public $route;

    public function mount(Model $model, $button, String $scope)
    {
        $this->model = $model;
        $this->scope = $scope;

        if (!is_array($button)){
            if ($theme = config('utils.datatable-buttons-themes.'.$button)){
                $this->params = $theme;
                $this->permission = $scope.".".$button;
                $this->route = $scope.".".$button;
            }
        }
        else{
            $this->params = $button;
            if (!isset($button['permission'])){
                $this->permission = $scope.".".$button;
                $this->route = $scope.".".$button;
            }
            else{
                $this->permission = $button['permission'];
                $this->route = $button['route'];
            }
        }
    }

    public function render()
    {
        return view('utils::livewire.datatable-button')->with([
            'model' => $this->model,
            'params' => $this->params,
            'scope' => $this->scope,
            'permission' => $this->permission,
            'route' => $this->route
        ]);
    }
}
