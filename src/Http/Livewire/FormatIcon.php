<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class FormatIcon extends Component
{
    public $filename;
    public $mimeType;

    public function mount($post = null){
        if(!is_null($post)){
            $filenameArray = explode('.', $post->filename);
            $this->mimeType = end($filenameArray);
        }
    }

    public function render()
    {
        return view('utils::livewire.format-icon')->with([
            'mimeType' => $this->mimeType
        ]);
    }
}
