<?php

namespace Bittacora\Utils\Http\Livewire;

use Livewire\Component;

class LanguagesWidget extends Component
{
    public string $format = 'flags';
    public string $layout = 'block';
    public array $extraData = [];

    public function render()
    {
        return view('utils::livewire.languages-widget')->with([
            'format' => $this->format,
            'layout' => $this->layout,
            'extraData' => $this->extraData
        ]);
    }
}
