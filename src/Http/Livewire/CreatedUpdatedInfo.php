<?php

namespace Bittacora\Utils\Http\Livewire;

use Bittacora\Bpanel4Users\Bpanel4UsersFacade;
use Bittacora\Utils\UtilsFacade;
use Illuminate\Database\Eloquent\Model;
use Livewire\Component;

class CreatedUpdatedInfo extends Component
{
    private ?string $createdBy = null;
    private ?string $updatedBy = null;
    private ?string $createdAt = null;
    private ?string $updatedAt = null;
    public Model $model;

    public function mount($createdAt = null, $updatedAt = null){
        if(!is_null($this->model->created_by) and isset($this->model->created_by)){
            $this->createdBy = Bpanel4UsersFacade::getNameById($this->model->created_by);
        }


        if(!is_null($this->model->updated_by) and isset($this->model->updated_by)){
            $this->updatedBy = Bpanel4UsersFacade::getNameById($this->model->updated_by);
        }

        if(!is_null($this->model->created_at) and isset($this->model->created_at)){
            $this->createdAt = UtilsFacade::timestamptoSpanishFormat($this->model->created_at);
        }

        if(!is_null($createdAt)){
            $this->createdAt = UtilsFacade::timestamptoSpanishFormat($createdAt);
        }

        if(!is_null($this->model->updated_at) and isset($this->model->updated_at)){
            $this->updatedAt = UtilsFacade::timestamptoSpanishFormat($this->model->updated_at);
        }

        if(!is_null($updatedAt)){
            $this->updatedAt = UtilsFacade::timestamptoSpanishFormat($updatedAt);
        }

    }

    public function render()
    {
        return view('utils::livewire.created-updated-info')->with([
            'createdBy' => $this->createdBy,
            'updatedBy' => $this->updatedBy,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt
        ]);
    }
}
